resource "aws_api_gateway_rest_api" "hello-lambda" {
  name = "hellolambda"
}

resource "aws_api_gateway_resource" "hello-lambda-resource" {
  parent_id = aws_api_gateway_rest_api.hello-lambda.root_resource_id
  path_part = "sum"
  rest_api_id = aws_api_gateway_rest_api.hello-lambda.id
}

resource "aws_api_gateway_method" "hello-lambda-get" {
  authorization = "NONE"
  http_method = "GET"
  resource_id = aws_api_gateway_resource.hello-lambda-resource.id
  rest_api_id = aws_api_gateway_rest_api.hello-lambda.id
}
