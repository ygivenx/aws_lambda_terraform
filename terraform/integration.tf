resource "aws_api_gateway_integration" "hello-lambda-integration" {
  http_method = aws_api_gateway_method.hello-lambda-get.http_method
  integration_http_method = "POST"
  resource_id = aws_api_gateway_resource.hello-lambda-resource.id
  rest_api_id = aws_api_gateway_rest_api.hello-lambda.id
  type = "AWS_PROXY"
  uri = aws_lambda_function.test_terra.invoke_arn
}

resource "aws_api_gateway_deployment" "hello-world-deploy" {
  depends_on  = [aws_api_gateway_integration.hello-lambda-integration]
  rest_api_id = aws_api_gateway_rest_api.hello-lambda.id
  stage_name  = "dev"
}

resource "aws_api_gateway_stage" "hello-world-stage" {
  stage_name    = "dev"
  rest_api_id   = aws_api_gateway_rest_api.hello-lambda.id
  deployment_id = aws_api_gateway_deployment.hello-world-deploy.id
}