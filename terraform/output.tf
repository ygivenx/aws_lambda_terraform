output "hello_lambda_endpoint" {
  value = aws_api_gateway_stage.hello-world-stage.invoke_url
}