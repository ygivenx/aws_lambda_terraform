"""
Handler Tests

"""
import json
import pytest
from src.handler import handler


def test_empty_event():
    """
    test basic flow

    :return: assertion
    """
    resp = handler({}, {})
    assert resp["statusCode"] == 200, "Correct response received"


@pytest.mark.parametrize("_input, output", [((1, 2), 3), ((1.1, 5.5), 6)])
def test_sum(_input, output):
    """
    Test if summation works

    """
    event = {
        "num1": _input[0],
        "num2": _input[1]
    }
    resp = handler(event, {})
    assert json.loads(resp["body"])["result"] == output, "Got expected output"
