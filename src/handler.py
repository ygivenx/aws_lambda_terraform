"""
Getting started with AWS Lambdas

"""
import json
import logging


def handler(event, context):
    """
    A simple lambda function

    :param event: The payload passed to the lambda
    :param context: Context information
    :return:
    """
    logging.info(context)

    _sum = None
    if "num1" in event and "num2" in event:
        _sum = int(event["num1"]) + int(event["num2"])
    elif event.get("queryStringParameters") is not None:
        print("Received event from API Gateway")
        query_params = event["queryStringParameters"]
        _sum = int(query_params["num1"]) + int(query_params["num2"])
    else:
        _sum = "Hello, World!"

    return {
        "statusCode": 200,
        "body": json.dumps({"result": _sum}),
        "isBase64Encoded": False
    }
